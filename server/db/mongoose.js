var mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose .connect(process.env.MONGOLAB_ONYX_URI);
module.exports = {mongoose};
