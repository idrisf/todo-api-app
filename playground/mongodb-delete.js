//const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/', (err, client) => {
  if (err) {
    return console.log('Unable to connect to MongoDB server');
  }

  console.log('Connected to MongoDB server');


  const db = client.db('TodoApp');

  // deleteMany
   // db.collection('Todos').deleteMany({text: 'eat lunch'}).then((result) => {
   //   console.log(result);
   // }, (err) => {
   //   console.log("could not delete todo", err);
   // });

  // deleteOne
  // db.collection("Todos").deleteOne({text: 'eat lunch'}).then((result) => {
  //   console.log(result);
  // })

  //  findOneAndDelete
  // db.collection('Todos').findOneAndDelete({completed: false}).then((result) => {
  //   console.log(result);
  // });
  //

// db.collection("Users").deleteMany({name: "Farhan"}).then((result) => {
//   console.log("delete many");
//   console.log(result);
// });
//
// db.collection("Users").findOneAndDelete({
//   _id: new ObjectID("5a80c0c1de5eb2c9af5e914b")
// }).then((result) => {
//   console.log("findOneAndDelete");
//   console.log(result);
// });


  //client.close();
});
