//const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/', (err, client) => {
  if (err) {
    return console.log('Unable to connect to MongoDB server');
  }

  const db = client.db('TodoApp');

  // db.collection('Todos').insertOne({
  //   text: 'Somethign to dodododo',
  //   completed: false
  // }, (err, result) => {
  //   if (err) {
  //     return cconsole.log('Unable to insert todo', err);
  //   }
  //
  //   console.log(JSON.stringify(result.ops, undefined, 2));
  // });

  // db.collection('Users').insertOne({
  //   name: 'Farhan',
  //   age: 25,
  //   location: 'Manchester'
  // }, (err, result) => {
  //   if (err) {
  //     return console.log('Unable to insert user', err);
  //   }
  //   console.log(result.ops[0]._id.getTimestamp());
  //
  // });

  console.log('Connected to MongoDB server');

  client.close();
});
