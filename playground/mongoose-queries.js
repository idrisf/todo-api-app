const {ObjectId} = require("mongodb");

const {mongoose} = require("./../server/db/mongoose");
const {Todo} = require("./../server/models/Todo");
const {User} = require("./../server/models/User");


// var id = '5a89b8d57ce30718ec68f8fb';
//
// if (!ObjectId.isValid(id)) {
//   console.log('id not valid');
// }
// Todo.find({
//   _id: id
// }).then((todos) => {
//   console.log('Todos', todos);
// });
//
// Todo.findOne({
//   _id: id
// }).then((todo) => {
//   console.log('Todo', todo);
// });

// Todo.findById(id).then((todo) => {
//     if (!todo) {
//       return console.log("id not found");
//     }
//     console.log('TodoById', todo);
// }).catch((e) => console.log(e));
//

var userId = "5a80cc0d933ab410b99e994b";

if (!ObjectId.isValid(userId)) {
  console.log('id not valid');
}

User.findById(userId).then((user) => {
  if (!user) {
    return console.log("user not found");
  }
  console.log("User", user);
}).catch((e) => console.log(e));
