const {ObjectId} = require("mongodb");

const {mongoose} = require("./../server/db/mongoose");
const {Todo} = require("./../server/models/Todo");
const {User} = require("./../server/models/User");

// Todo.remove({}).then((result) => {
//   console.log(result);
// });

// Todo.findOneAndRemove()

Todo.findOneAndRemove({_id: '5a89edebde5eb2c9af642149'}).then((todo) => {
  console.log(todo);
})

//Todo.findByIdAndRemove

Todo.findByIdAndRemove('5a89edebde5eb2c9af642149').then((todo) => {
  console.log(todo);
});
