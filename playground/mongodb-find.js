//const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/', (err, client) => {
  if (err) {
    return console.log('Unable to connect to MongoDB server');
  }

  const db = client.db('TodoApp');

  // db.collection("Todos").find({
  //   _id: new ObjectID("5a80a47e3716e00c5e669aff")
  // }).toArray().then((docs) => {
  //   console.log("Todos");
  //   console.log(JSON.stringify(docs, undefined, 2));
  // }, (err) => {
  //   console.log("unable to get todos", err)
  // });

  // db.collection("Todos").find().count().then((count) => {
  //   console.log(`Todos count: ${count}`);
  // }, (err) => {
  //   console.log("unable to get todos", err)
  // });

  db.collection("Users").find({name: "Farhan"}).toArray().then((docs) => {
    console.log("Todo list for Farhan");
    console.log(JSON.stringify(docs, undefined, 2));
  }, (err) => {
    console.log("Unable to get todos", err);
  })

  console.log('Connected to MongoDB server');

  //client.close();
});
